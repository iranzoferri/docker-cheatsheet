# docker-compose cheatsheet

Fuente: [devhints.io]
(https://devhints.io/docker-compose)

---
### Basic example

###### # docker-compose.yml
```yaml
version: '2'

services:
  web:
    build: .
    # build from Dockerfile
    context: ./Path
    dockerfile: Dockerfile
    ports:
    - "5000:5000"
    volumes:
    - .:/code
  redis:
    image: redis
```

### Commands
```yaml
docker-compose start
docker-compose stop

docker-compose pause
docker-compose unpause

docker-compose ps
docker-compose up
docker-compose down
```
----
## #Reference



### Building



###### # build from Dockerfile
```yaml
web:
  build: .
  args:     # Add build arguments
    APP_HOME: app
```

###### # build from custom Dockerfile
```yaml
  build:
    context: ./dir
    dockerfile: Dockerfile.dev
```

###### # build from image
```yaml
  image: ubuntu
  image: ubuntu:14.04
  image: tutum/influxdb
  image: example-registry:4000/postgresql
  image: a4bc65fd
```


### Ports
```yaml
ports:
  - "3000"
  - "8000:80"  # host:container
# expose ports to linked services (not to host)
expose: ["3000"]
```


### Commands



###### # command to execute
```yaml
command: bundle exec thin -p 3000
command: [bundle, exec, thin, -p, 3000]
```

###### # override the entrypoint
```yaml
entrypoint: /app/start.sh
entrypoint: [php, -d, vendor/bin/phpunit]
```


### Environment variables



###### # environment vars
```yaml
environment:
  RACK_ENV: development
environment:
  - RACK_ENV=development
```
###### # environment vars from file
```yaml
env_file: .env
env_file: [.env, .development.env]
```


### Dependences



###### # makes the `db` service available as the hostname `database` (implies depends_on)
```yaml
links:
  - db:database
  - redis
```

###### # make sure `db` is alive before starting
```yaml
depends_on:
  - db
```



----
## #Advanced features

### Labels
```yaml
services:
  web:
    labels:
      com.example.description: "Accounting web app"
```

### DNS servers
```yaml
services:
  web:
    dns: 8.8.8.8
    dns:
      - 8.8.8.8
      - 8.8.4.4
```
### Devices
```yaml
services:
  web:
    devices:
    - "/dev/ttyUSB0:/dev/ttyUSB0"

### ```Hosts
```yaml
services:
  web:
    extra_hosts:
      - "somehost:192.168.1.100"
```
### Network

###### # creates a custom network called `frontend`
```yaml
networks:
  frontend:
```
### External links
```yaml
services:
  web:
    external_links:
      - redis_1
      - project_db_1:mysql
```
### External network

###### # join a pre-existing network
```yaml
networks:
  default:
    external:
      name: frontend
```
### Volume

###### # Mount host paths or named volumes, specified as sub-options to a service
```yaml
  db:
    image: postgres:latest
    volumes:
      - "/var/run/postgres/postgres.sock:/var/run/postgres/postgres.sock"
      - "dbdata:/var/lib/postgresql/data"

volumes:
  dbdata:
```

